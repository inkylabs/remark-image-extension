import visit from 'unist-util-visit'

export default (opts) => {
  opts = Object.assign({
    ext: ''
  }, opts)

  return (root, f) => {
    visit(root, 'image', (node, index, parent) => {
      if (node.url.match(/\..{2,4}$/)) return
      node.url += '.' + opts.ext
    })
  }
}
